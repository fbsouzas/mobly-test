<?php

namespace AppBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Product;

class CartManager
{
    protected $container;
    protected $em;

    /**
     * construct
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
        $this->session = $this->container->get('session');
    }

    /**
     * Add new product in session
     *
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $this->session->get('products')
            ? $products = $this->session->get('products')
            : $products = array();

        $products[$product->getId()] = $product;
        $this->session->set('products', $products);
    }

    /**
     * Return all products in session
     *
     * @return session
     */
    public function getProducts()
    {
        return $this->session->get('products');
    }

    /**
     * Delete a product of session
     *
     * @param roduct $product
     */
    public function deleteProduct(Product $product)
    {
        $products = $this->session->get('products');
        unset($products[$product->getId()]);

        $this->session->set('products', $products);
    }

    /**
     * Return quantity of products in session
     *
     * @return interger
     */
    public function getQuantitiesProducts()
    {
        return count($this->session->get('products'));
    }

    /**
     * Return amount of products
     *
     * @return decimal
     */
    public function getAmount()
    {
        $products = $this->session->get('products')
            ? $this->session->get('products')
            : array();

        $prices = array();
        foreach ($products as $key => $product) {
            array_push($prices, $product->getPrice());
        }

        return array_sum($prices);
    }

    /**
     * Clean products session
     */
    public function clean()
    {
        $this->session->remove('products');
    }
}