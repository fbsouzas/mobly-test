<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;

/**
 * Cart controller.
 *
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * Lists all Product in session cart.
     *
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('cart/index.html.twig', array());
    }

    /**
     * Creates a new Product in session cart.
     *
     * @Route("/{id}/new")
     * @Method("GET")
     */
    public function newAction(Product $product)
    {
        $cartManager = $this->get('app.cart_manager');
        $cartManager->addProduct($product);

        $this->addFlash('notice', 'New registry of cart success');

        return $this->redirectToRoute('app_cart_index');
    }

    /**
     * Deletes a Product of session cart.
     *
     * @Route("/{id}")
     * @Method("GET")
     */
    public function deleteAction(Product $product)
    {
        $cartManager = $this->get('app.cart_manager');
        $cartManager->deleteProduct($product);

        $this->addFlash('notice', 'Delete registry of cart success');

        return $this->redirectToRoute('app_cart_index');
    }
}
