<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Checkout;
use AppBundle\Form\CheckoutType;

/**
 * Checkout controller.
 *
 * @Route("/checkout")
 */
class CheckoutController extends Controller
{
    /**
     * Lists all Checkout entities.
     *
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $checkouts = $em->getRepository('AppBundle:Checkout')->findAllOrderedByCreateAt();

        return $this->render('checkout/index.html.twig', array(
            'checkouts' => $checkouts,
        ));
    }

    /**
     * Creates a new Checkout entity.
     *
     * @Route("/new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $checkout = new Checkout();
        $form = $this->createForm('AppBundle\Form\CheckoutType', $checkout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cartManager = $this->get('app.cart_manager');

            foreach ($cartManager->getProducts() as $key => $productInSession) {
                $product = $em->getRepository('AppBundle:Product')->findOneById($productInSession->getId());
                $checkout->addProduct($product);
            }

            $cartManager->clean();
            $em->persist($checkout);
            $em->flush();

            $this->addFlash('notice', 'New registry success');

            return $this->redirectToRoute('app_checkout_index');
        }

        return $this->render('checkout/new.html.twig', array(
            'checkout' => $checkout,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Checkout entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     */
    public function showAction(Checkout $checkout)
    {
        $deleteForm = $this->createDeleteForm($checkout);

        return $this->render('checkout/show.html.twig', array(
            'checkout' => $checkout,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Checkout entity.
     *
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Checkout $checkout)
    {
        $deleteForm = $this->createDeleteForm($checkout);
        $editForm = $this->createForm('AppBundle\Form\CheckoutType', $checkout);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($checkout);
            $em->flush();

            $this->addFlash('notice', 'Edit registry success');

            return $this->redirectToRoute('app_checkout_index');
        }

        return $this->render('checkout/edit.html.twig', array(
            'checkout' => $checkout,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Checkout entity.
     *
     * @Route("/{id}")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Checkout $checkout)
    {
        $form = $this->createDeleteForm($checkout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($checkout);
            $em->flush();

            $this->addFlash('notice', 'Delete registry success');
        }

        return $this->redirectToRoute('app_checkout_index');
    }

    /**
     * Creates a form to delete a Checkout entity.
     *
     * @param Checkout $checkout The Checkout entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Checkout $checkout)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_checkout_delete', array('id' => $checkout->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
