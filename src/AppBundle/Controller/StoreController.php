<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;

/**
 * list all products
 */
class StoreController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createFilterForm(
            $em->getRepository('AppBundle:Category')->findAllForFilter()
        );

        if ($request->query->has('cat') || $request->query->has('q')) {
            $form->handleRequest(null, $request);
        }

        $data = $form->getData();
        $query = $em->getRepository('AppBundle:Product')->findByCategoryAndName($data);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('store/index.html.twig', array(
            'products' => $pagination,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Product entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     */
    public function showAction(Product $product)
    {
        return $this->render('store/show.html.twig', array(
            'product' => $product,
        ));
    }

    /**
     * Creates a form to filter a InitiativeRegister entity.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm(Array $categories)
    {
        $formFactory = Forms::createFormFactory();

        return $formFactory->createNamedBuilder(null, FormType::class, null, array(
                'action' => $this->generateUrl('app_store_index'),
                'method' => 'GET',
            ))
            ->add('cat', ChoiceType::class, array(
                'choices'  => $categories,
                'label' => 'label.category',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ))
            ->add('q', TextType::class, array(
                'label' => 'label.search',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->getForm()
        ;
    }
}
