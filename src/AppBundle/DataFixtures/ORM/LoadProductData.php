<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Product;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $product = new Product();
        $product->setName('Sofá 3 Lugares Retrátil e Reclinável Eureka Suede Mojave');
        $product->setDescription('Coloque conforto em sua sala e deixe a decoração comum toque elegante e muito charmoso. O Sofá Eureka é a definição perfeita de aconchego para receber visitas ou tirar aquele descanso. Retrátil e reclinável, ele permite acomodar você e sua família da melhor maneira possível em 3 lugares, para que vocês assistam aos seus programas favoritos! Ah! A cor mojave em tecido suede, combina harmoniosamente com toda a decoração, deixando o seu lar receptivo e ainda mais bonito. =D');
        $product->setPrice('999.99');
        $product->setImage('74b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Black');
        $product->setSize('100cmx60cm');
        $product->addCategory($this->getReference('category-1'));
        $product->addCategory($this->getReference('category-4'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Sofá 3 Lugares Retrátil e Reclinável Plaza Suede Café Linoforte');
        $product->setDescription('Para você, elegancia é indispensável na decoração? Então você precisa ter este magnífico Sofá Plaza na sua sala! Dono de um design moderno e de muito bom gosto, ele garantirá pleno conforto a você e às suas visitas, pois além de ser bastante macio, também conta com encosto reclinável assentos retráteis, permitindo que você estique as pernas e se sinta ainda mais à vontade. Demais, não é mesmo?');
        $product->setPrice('699.99');
        $product->setImage('94b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Red');
        $product->setSize('100cmx60cm');
        $product->addCategory($this->getReference('category-1'));
        $product->addCategory($this->getReference('category-4'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Puff Fofão em Courino Preto');
        $product->setDescription('Um novo conceito de conforto e design para os móveis da sua casa. Se você quer deixar a sala ou o quarto com um estilo arrojado, moderno e descontraído, o Puff Fofão é ideal para decorar o ambiente da melhor forma e ainda garante versatilidade para você receber seus convidados com comodidade.

            Além de apresentar facilidade na limpeza, o revestimento em courino do puff oferece resistência e valoriza uma decoração mais sofisticada ao espaço. Seu preenchimento com flocos de isopor proporciona maciez, assegurando o aconchego de todos. Com a cor preto, este puff se encaixa em qualquer ambiente com muita harmonia e discrição.

            Líder no setor pela sua ampla variedade de modelos, a Phoenix Puff busca superar as expectativas dos seus consumidores através de um espaço inovador e criativo. Por isso, o Puff Fofão em Courino possui a estrutura de um móvel confortável e moderno para a sua casa.');
        $product->setPrice('139.99');
        $product->setImage('44b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Green');
        $product->setSize('200cmx120cm');
        $product->addCategory($this->getReference('category-2'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Sofá-Cama Casal Premium Suede Castanho');
        $product->setDescription('A sua casa merece um sofá bonito e de qualidade para receber suas visitas e acomodar toda a família confortavelmente, não é verdade? Melhor ainda se o sofá se transformar em uma aconchegante cama, como o Sofá-Cama Premium. Ele conta com um design atraente na cor castanho e possui um agradável revestimento em suede. Prático, né? ;)');
        $product->setPrice('999.99');
        $product->setImage('84b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Blue');
        $product->setSize('220cmx120cm');
        $product->addCategory($this->getReference('category-3'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Sofá-Cama e Chaise Casal Versátil Veludo Liso Amarelo');
        $product->setDescription('Se você está precisando de uma cama extra que complete a decoração com muito estilo, que tal contar com este Sofá-Cama e Chaise Versátil? Ele é ótimo para utilizar o cômodo de maneira inteligente e garantir versatilidade ao espaço. Ele ainda conta com o encosto reclinável na função cama. Perfeito, não? Seja como cama ou como sofá, conforto é o que não vai faltar para suas atividades do dia a dia, desde ler um livro até para as noites de sono. Simples, prático e extremamente útil! Aproveite! ; )');
        $product->setPrice('1399.99');
        $product->setImage('04b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Blue');
        $product->setSize('220cmx120cm');
        $product->addCategory($this->getReference('category-3'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Poltrona Sabrina Suede Bege Mobisul');
        $product->setDescription('Já imaginou uma poltrona linda e confortável que agrega beleza à decoração e ainda faz com que os seus momentos sejam mais prazerosos? A Poltrona Sabrina é uma excelente opção para deixar qualquer ambiente mais aconchegante, charmoso e sofisticado, podendo ser usada na sala de estar, no quarto ou no escritório. Um arraso, né? ;)');
        $product->setPrice('199.99');
        $product->setImage('34b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Yellow');
        $product->setSize('200cmx120cm');
        $product->addCategory($this->getReference('category-4'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Rack 1.6 Retrô 50 Branco');
        $product->setDescription('Olhe que irresistível! Tem como não se apaixonar por este Rack Retrô 50?! Ele vai ficar um arraso na sua sala de estar! Com todo seu charme retrô, não tem como errar na decoração do ambiente. Você surpreenderá todas as suas visitas com muito bom gosto e um estilo único! Bastante espaçoso, nele você poderá organizar sua TV, aparelhos de DVD e muito mais. Aproveite! :)');
        $product->setPrice('289.99');
        $product->setImage('54b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Blue');
        $product->setSize('200cmx120cm');
        $product->addCategory($this->getReference('category-5'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Rack Oasis Canela e Turquesa');
        $product->setDescription('Aproveite e complete a decoração da sua sala de estar com a modernidade do Rack Oasis! O móvel é perfeito para completar o espaço com beleza e charme, contando com espaço para TV, objetos de decoração e aparelhos eletrônicos. Seu design moderno conta com pés no estilo retrô, garantindo assim que a redecoração do cômodo fique um encanto. Show, né? ;)');
        $product->setPrice('359.99');
        $product->setImage('64b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Black');
        $product->setSize('120cmx80cm');
        $product->addCategory($this->getReference('category-5'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Estante para Home Theater Allure 2.1 Branco e Canyon HB Móveis');
        $product->setDescription('Olha que lindo! Um móvel de personalidade e muito estilo para complementar a sua casa! Esta Estante para Home Theater Allure possui um design moderno e pra lá de elegante para que a sua sala de estar fique com o visual impecável. Além de muito bonito, ela também conta com bastante espaço para que você possa organizar os seus pertences, desde pequenos enfeites até sua TV. Não é o máximo? :)');
        $product->setPrice('999.99');
        $product->setImage('14b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Black');
        $product->setSize('120cmx80cm');
        $product->addCategory($this->getReference('category-6'));
        $manager->persist($product);

        $product = new Product();
        $product->setName('Mesa de Centro Tok Branca');
        $product->setDescription('Quer dar uma nova cara para o seu lar com mudanças pequenas, porém significativas? Pois então comece com a Mesa de Centro Tok. Bonita e resistente, ela dá aquela repaginada no ambiente e, de quebra, acomoda seus objetos de decoração. Um mimo, né?');
        $product->setPrice('149.99');
        $product->setImage('24b1d2488b8e59e90aa6487d42dfc471.jpg');
        $product->setColor('Red');
        $product->setSize('220cmx120cm');
        $product->addCategory($this->getReference('category-7'));
        $manager->persist($product);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 2;
    }
}
