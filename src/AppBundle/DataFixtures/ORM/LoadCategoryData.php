<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setName('Sofás');
        $this->addReference('category-1', $category);
        $manager->persist($category);

        $category = new Category();
        $category->setName('puffs');
        $this->addReference('category-2', $category);
        $manager->persist($category);

        $category = new Category();
        $category->setName('Sofás-cama');
        $this->addReference('category-3', $category);
        $manager->persist($category);

        $category = new Category();
        $category->setName('Poltronas');
        $this->addReference('category-4', $category);
        $manager->persist($category);

        $category = new Category();
        $category->setName('Racks');
        $this->addReference('category-5', $category);
        $manager->persist($category);

        $category = new Category();
        $category->setName('Estantes');
        $this->addReference('category-6', $category);
        $manager->persist($category);

        $category = new Category();
        $category->setName('Mesas de centro');
        $this->addReference('category-7', $category);
        $manager->persist($category);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 1;
    }
}
