<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CheckoutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'label.name',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => 'label.phone',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('address', TextType::class, array(
                'label' => 'label.address',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('city', TextType::class, array(
                'label' => 'label.city',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('state', TextType::class, array(
                'label' => 'label.state',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Checkout'
        ));
    }
}
