<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'label.name',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'label.description',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('image', FileType::class, array(
                'label' => 'label.image',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('price', MoneyType::class, array(
                'label' => 'label.price',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('color', ChoiceType::class, array(
                'label' => 'label.color',
                'choices'  => array(
                    'Black' => 'Black',
                    'Yellow' => 'Yellow',
                    'Red' => 'Red',
                    'Blue' => 'Blue',
                    'Green' => 'Green',
                ),
                'placeholder' => 'label.empty_value',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('size', ChoiceType::class, array(
                'label' => 'label.size',
                'choices'  => array(
                    '100cmx60cm' => '100cmx60cm',
                    '120cmx80cm' => '120cmx80cm',
                    '150cmx100cm' => '150cmx100cm',
                    '200cmx120cm' => '200cmx120cm',
                    '220cmx120cm' => '220cmx120cm',
                ),
                'placeholder' => 'label.empty_value',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('categories', EntityType::class, array(
                'class' => 'AppBundle:Category',
                'choice_label' => 'name',
                'label' => 'label.category',
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }
}
