# Test Fábio Souza
### fsouza.me@gmail.com

## Instalation

### Steps

### 1. Clone the project
```sh
git clone git@bitbucket.org:fbsouzas/mobly-test.git
```

### 2. Go to the project directory
```sh
cd /path/to/project/
```

### 3. Install the dependencies
```sh
composer install
```
(If not has **composer** visit [getcomposer.org](http://getcomposer.org) for more details)

### 4. Create database tables
```sh
php bin/console doc:sch:up --force
```

### 5. Load data fixtures for tests
```sh
php bin/console doctrine:fixtures:load
```

### 6. Started symfony internal PHP web server
```sh
php bin/console server:run
```

### 7. Open your browser and access
```sh
http://localhost:8000/
```

### Observations
* I just add images in my commits to facilitate testing
* For creates more products, access: /product/new